# RPG Simulated Collaboration

Pretend collaboration with an inexistent developer working on the task while I work on characters.

- An infinite scaling tower.
- Each floor will have monsters of selected difficulty for each floor.
- Each monster will drop loot or gold, loot can be equipped or sold.
- After every floor the party(4 members) can go to town to heal up or buy equipment.

- Each character has these methods including their own method depending on their character:
    - getCurrentMaxHealth()
    - getCurrentHealth()
    - setCurrentHealth()
    - setShield()
    - getDead()
    - equipArmor()
    - equipWeapon()
    - takeDamage()

- Each character can only equip armor and weapon that fits to their classes.

- Factories create weapons, armor and characters with random common equipment and skills that fits to their classes.

## Character setup:

- Hero:
    - HealSupport: Druid
    - ShieldSupport: Priest
    - Caster damage: mage, warlock
    - BluntMelee damage: paladin
    - BladedMelee damage: rogue, warrior
    - Ranged: ranger

- Weapon:
    - Magic: wands, staffs
    - Blunt: hammers, maces
    - Ranged: bow, crossbow, gun
    - Bladed: axe, dagger, sword

- Armor:
    - Cloth, leather, plate, mail