package main.java.spells.shielding;

import main.java.basestats.SpellModifiers;
import main.java.spells.abstractions.ShieldingSpell;

public class Rapture implements ShieldingSpell {

    @Override
    public double getAbsorbShieldPercentage() {
        return SpellModifiers.RAPTURE_SHIELD_OF_MAX_HEALTH;
    }

    @Override
    public String getSpellName() {
        return "Rapture";
    }
}
