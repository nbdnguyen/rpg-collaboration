package main.java.characters;
// Imports
import main.java.basestats.CharacterBaseStatsDefensive;
import main.java.basestats.CharacterBaseStatsOffensive;
import main.java.characters.abstractions.Caster;
import main.java.items.armor.abstractions.Armor;
import main.java.items.armor.Cloth;
import main.java.items.weapons.abstractions.Weapon;
import main.java.items.weapons.abstractions.Magic;
import main.java.spells.abstractions.DamagingSpell;

/*
 Class description:
 ------------------
 Mages are masters of arcane magic. Their skills are honed after years of dedicated study.
 They conjure arcane energy to deal large amounts of damage to enemies.
 They are vulnerable to physical attacks but are resistant to magic.
*/
public class Mage implements Caster {
    // Metadata
    private DamagingSpell damagingSpell;

    // Base stats defensive
    private double baseHealth = CharacterBaseStatsDefensive.MAGE_BASE_HEALTH;
    private double basePhysReductionPercent = CharacterBaseStatsDefensive.MAGE_BASE_PHYS_RED; // Armor
    private double baseMagicReductionPercent = CharacterBaseStatsDefensive.MAGE_BASE_MAGIC_RES; // Magic armor
    // Base stats offensive
    private double baseMagicPower = CharacterBaseStatsOffensive.MAGE_MAGIC_POWER;

    // Active trackers and flags
    private double currentHealth;
    private Boolean isDead = false;
    private Armor armor = null;
    private Weapon weapon = null;
    private double shield = 0;

    // Constructor
    public Mage() {
        // When the character is created it has maximum health (base health)
        this.currentHealth = baseHealth;
    }

    public Mage(Armor armor, Weapon weapon, DamagingSpell damagingSpell) {
        this.currentHealth = baseHealth;
        this.armor = armor;
        this.weapon = weapon;
        this.damagingSpell = damagingSpell;
    }

    // Public getters statuses and stats
    public double getCurrentMaxHealth() {
        if (armor == null) {
            return baseHealth;
        } else {
            return baseHealth*armor.getHealthModifier()*armor.getRarityModifier();

        }
    }

    public void setShield(double shieldPoints) {
        shield = shieldPoints;
    }

    public double getCurrentHealth() {
        return currentHealth;
    }

    public void setCurrentHealth(double healPoints) {
        if (currentHealth + healPoints > this.getCurrentMaxHealth()) {
            currentHealth = this.getCurrentMaxHealth();
        } else {
            currentHealth += healPoints;
        }
    }

    public Boolean getDead() {
        return isDead;
    }

    // Equipment behaviours

    /**
     * Equips armor to the character, modifying stats.
     * @param armor
     */
    public void equipArmor(Armor armor) {
        if (armor instanceof Cloth) {
            this.armor = armor;
            this.baseHealth = CharacterBaseStatsDefensive.MAGE_BASE_HEALTH*armor.getHealthModifier();
        } else {
            System.out.println("Not equipped. Mage only equips cloth armor!");
        }

    }

    /**
     * Equips a weapon to the character, modifying stats.
     * @param weapon
     */
    public void equipWeapon(Weapon weapon) {
        if (weapon instanceof Magic) {
            this.weapon = weapon;
        } else {
            System.out.println("Not equipped. Mage only uses magic weapons.");
        }
    }

    // Character behaviours

    /**
     * Damages the enemy with its spells
     */
    public double castDamagingSpell() {
        return baseMagicPower*weapon.getModifier()*damagingSpell.getSpellDamageModifier();
    }

    /**
     * Takes damage from an enemy's attack.
     * @param incomingDamage
     * @Param damageType
     */
    public double takeDamage(double incomingDamage, String damageType) {
        double damage = 0;
        if (damageType.equals("physical")) {
            damage = incomingDamage*(1-basePhysReductionPercent*armor.getPhysRedModifier()*armor.getRarityModifier());
        } else if (damageType.equals("magical")) {
            damage = incomingDamage*(1-baseMagicReductionPercent*armor.getMagicRedModifier()*armor.getRarityModifier());
        }
        if (damage < 1 ) {
            damage = 1;
        }
        double curDamage = damage;
        if (shield > 0) {
            if (shield > curDamage) {
                shield -= curDamage;
            } else {
                curDamage -= shield;
                shield = 0;
                currentHealth -= curDamage;
            }
        } else {
            currentHealth -= curDamage;
        }
        if (currentHealth <= 0) {
            isDead = true;
        }
        return damage;
    }
}
