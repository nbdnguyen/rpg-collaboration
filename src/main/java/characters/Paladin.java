package main.java.characters;
// Imports
import main.java.basestats.CharacterBaseStatsDefensive;
import main.java.basestats.CharacterBaseStatsOffensive;
import main.java.characters.abstractions.BluntMelee;
import main.java.items.armor.abstractions.Armor;
import main.java.items.armor.abstractions.ArmorType;
import main.java.items.armor.Plate;
import main.java.items.weapons.abstractions.Weapon;
import main.java.items.weapons.abstractions.Blunt;

/*
 Paladins are faithful servants of the light and everything holy.
 They dispatch justice and are filled with vengeance for the wrongs done to society by evil.
 Paladins are very durable and typically wield heavy weapons.
*/
public class Paladin implements BluntMelee {
    // Metadata
    public final ArmorType ARMOR_TYPE = ArmorType.Plate;

    // Base stats defensive
    private double baseHealth = CharacterBaseStatsDefensive.PALADIN_BASE_HEALTH;
    private double basePhysReductionPercent = CharacterBaseStatsDefensive.PALADIN_BASE_PHYS_RED; // Armor
    private double baseMagicReductionPercent = CharacterBaseStatsDefensive.PALADIN_BASE_MAGIC_RES; // Magic armor
    // Base stats offensive
    private double baseAttackPower = CharacterBaseStatsOffensive.PALADIN_MELEE_ATTACK_POWER;

    // Active trackers and flags
    private double currentHealth;
    private Boolean isDead = false;
    private Armor armor = null;
    private Weapon weapon = null;
    private double shield = 0;

    // Constructor
    public Paladin() {
        // When the character is created it has maximum health (base health)
        this.currentHealth = baseHealth;
    }

    public Paladin(Armor armor, Weapon weapon) {
        this.currentHealth = baseHealth;
        this.armor = armor;
        this.weapon = weapon;
    }

    // Public getters statuses and stats
    public double getCurrentMaxHealth() {
        if (armor == null) {
            return baseHealth;
        } else {
            return baseHealth*armor.getHealthModifier()*armor.getRarityModifier();
        }
    }

    public void setShield(double shieldPoints) {
        shield = shieldPoints;
    }

    public double getCurrentHealth() {
        return currentHealth;
    }

    public void setCurrentHealth(double healPoints) {
        if (currentHealth + healPoints > this.getCurrentMaxHealth()) {
            currentHealth = this.getCurrentMaxHealth();
        } else {
            currentHealth += healPoints;
        }
    }

    public Boolean getDead() {
        return isDead;
    }

    // Equipment behaviours

    /**
     * Equips armor to the character, modifying stats.
     * @param armor
     */
    public void equipArmor(Armor armor) {
        if (armor instanceof Plate) {
            this.armor = armor;
            this.currentHealth = this.currentHealth*armor.getHealthModifier();
        } else {
            System.out.println("Not eqipped. Paladin only wears plate armor!");
        }
    }

    /**
     * Equips a weapon to the character, modifying stats.
     * @param weapon
     */
    public void equipWeapon(Weapon weapon) {
        if (weapon instanceof Blunt) {
            this.weapon = weapon;
        } else {
            System.out.println("Not equipped. Paladin only uses blunt weapons.");
        }
    }

    // Character behaviours

    /**
     * Damages the enemy
     */
    public double attackWithBluntWeapon() {
        return baseAttackPower*weapon.getModifier()*weapon.getRarity();
    }

    /**
     * Takes damage from an enemy's attack.
     * @param incomingDamage
     * @Param damageType
     */
    public double takeDamage(double incomingDamage, String damageType) {
        double damage = 0;
        if (damageType.equals("physical")) {
            damage = incomingDamage*(1-basePhysReductionPercent*armor.getPhysRedModifier()*armor.getRarityModifier());
        } else if (damageType.equals("magical")) {
            damage = incomingDamage*(1-baseMagicReductionPercent*armor.getMagicRedModifier()*armor.getRarityModifier());
        }
        if (damage < 1 ) {
            damage = 1;
        }
        double curDamage = damage;
        if (shield > 0) {
            if (shield > curDamage) {
                shield -= curDamage;
            } else {
                curDamage -= shield;
                shield = 0;
                currentHealth -= curDamage;
            }
        } else {
            currentHealth -= curDamage;
        }
        if (currentHealth <= 0) {
            isDead = true;
        }
        return damage;
    }
}
