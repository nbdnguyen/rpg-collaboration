package main.java.characters.abstractions;

public interface HealSupport extends Hero {
    double healPartyMember(Hero partyMember);
}
