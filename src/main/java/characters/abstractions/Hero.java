package main.java.characters.abstractions;

import main.java.items.armor.abstractions.Armor;
import main.java.items.weapons.abstractions.Weapon;

public interface Hero {
    double getCurrentMaxHealth();
    double getCurrentHealth();
    void setCurrentHealth(double healPoints);
    Boolean getDead();
    void equipArmor(Armor armor);
    void equipWeapon(Weapon weapon);
    double takeDamage(double incomingDamage, String damageType);
    void setShield(double shielding);
}
