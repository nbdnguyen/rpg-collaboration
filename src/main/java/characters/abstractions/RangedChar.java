package main.java.characters.abstractions;

public interface RangedChar extends Hero {
    double attackWithRangedWeapon();
}
