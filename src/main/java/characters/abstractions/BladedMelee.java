package main.java.characters.abstractions;


public interface BladedMelee extends Hero {
    double attackWithBladedWeapon();
}
