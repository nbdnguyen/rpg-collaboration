package main.java.characters.abstractions;

public interface BluntMelee extends Hero{
    double attackWithBluntWeapon();
}
