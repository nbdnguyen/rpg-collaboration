package main.java.characters.abstractions;

public interface ShieldSupport extends Hero {
    double shieldPartyMember(Hero partyMember);
}
