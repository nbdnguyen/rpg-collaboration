package main.java.characters.abstractions;

public interface Caster extends Hero {
    double castDamagingSpell();
}