package main.java.characters;
// Imports
import main.java.basestats.CharacterBaseStatsDefensive;
import main.java.characters.abstractions.HealSupport;
import main.java.characters.abstractions.Hero;
import main.java.items.armor.abstractions.Armor;
import main.java.items.armor.Leather;
import main.java.items.weapons.abstractions.Weapon;
import main.java.items.weapons.abstractions.Magic;
import main.java.spells.abstractions.HealingSpell;

/*
 Class description:
 ------------------
 Druids are spell casters who use nature based magic to aid their allies in battle.
 They can heal their allies or protect them using the forces of nature.
 As a support class they only have defensive stats.
*/
public class Druid implements HealSupport {
    // Metadata
    private HealingSpell healingSpell;

    // Base stats defensive
    private double baseHealth = CharacterBaseStatsDefensive.DRUID_BASE_HEALTH;
    private double basePhysReductionPercent = CharacterBaseStatsDefensive.DRUID_BASE_PHYS_RED; // Armor
    private double baseMagicReductionPercent = CharacterBaseStatsDefensive.DRUID_BASE_MAGIC_RES; // Magic armor

    // Active trackers and flags
    private double currentHealth;
    private Boolean isDead = false;
    private Armor armor = null;
    private Weapon weapon = null;
    private double shield = 0;

    // Constructor
    public Druid() {
        // When the character is created it has maximum health (base health)
        this.currentHealth = baseHealth;
    }

    public Druid(Armor armor, Weapon weapon, HealingSpell healingSpell) {
        this.currentHealth = baseHealth;
        this.armor = armor;
        this.weapon = weapon;
        this.healingSpell = healingSpell;
    }

    // Public getters statuses and stats
    public double getCurrentMaxHealth() {
        if (armor == null) {
            return baseHealth;
        } else {
            return baseHealth*armor.getHealthModifier()*armor.getRarityModifier();
        }
    }

    public double getCurrentHealth() {
        return currentHealth;
    }

    public void setCurrentHealth(double healPoints) {
        if (currentHealth + healPoints > this.getCurrentMaxHealth()) {
            currentHealth = this.getCurrentMaxHealth();
        } else {
            currentHealth += healPoints;
        }
    }

    public void setShield(double shieldPoints) {
        //Shield does not stack, so just resets.
        shield = shieldPoints;
    }

    public Boolean getDead() {
        return isDead;
    }

    // Equipment behaviours

    /**
     * Equips armor to the character, modifying stats.
     * @param armor
     */
    public void equipArmor(Armor armor) {
        if (armor instanceof Leather) {
            this.armor = armor;
            this.currentHealth = this.currentHealth*armor.getHealthModifier();
        } else {
            System.out.println("Not equipped. Druid only equips leather armor!");
        }
        //this.baseMagicReductionPercent = armor.getMagicRedModifier();
        //this.basePhysReductionPercent = armor.getPhysRedModifier();
    }

    /**
     * Equips a weapon to the character, modifying stats
     * @param weapon
     */
    public void equipWeapon(Weapon weapon) {
        if (weapon instanceof Magic) {
            this.weapon = weapon;
        } else {
            System.out.println("Not equipped. Druid only uses magic weapons.");
        }
    }

    // Character behaviours

    /**
     * Heals the party member
     */
    public double healPartyMember(Hero partyMember) {
        double heal = healingSpell.getHealingAmount()*weapon.getModifier()*weapon.getRarity();
        partyMember.setCurrentHealth(heal);
        return heal;
    }

    /**
     * Takes damage from an enemy's attack.
     * @param incomingDamage
     * @Param damageType
     */
    public double takeDamage(double incomingDamage, String damageType) {
        double damage = 0;
        if (damageType.equals("physical")) {
            damage = incomingDamage*(1-basePhysReductionPercent*armor.getPhysRedModifier()*armor.getRarityModifier());
        } else if (damageType.equals("magical")) {
            damage = incomingDamage*(1-baseMagicReductionPercent*armor.getMagicRedModifier()*armor.getRarityModifier());
        }
        if (damage < 1 ) {
            damage = 1;
        }
        double curDamage = damage;
        if (shield > 0) {
            if (shield > curDamage) {
                shield -= curDamage;
            } else {
                curDamage -= shield;
                shield = 0;
                currentHealth -= curDamage;
            }
        } else {
            currentHealth -= curDamage;
        }
        if (currentHealth <= 0) {
            isDead = true;
        }
        return damage;
    }
}
