package main.java.characters;
// Imports
import main.java.basestats.CharacterBaseStatsDefensive;
import main.java.basestats.CharacterBaseStatsOffensive;
import main.java.characters.abstractions.BladedMelee;
import main.java.items.armor.abstractions.Armor;
import main.java.items.armor.abstractions.ArmorType;
import main.java.items.armor.Leather;
import main.java.items.weapons.abstractions.Weapon;
import main.java.items.weapons.abstractions.Bladed;
/*
 Rogues are stealthy combatants of the shadows.
 They wield bladed weapons with great agility and dispatch their enemies swiftly.
*/
public class Rogue implements BladedMelee {
    // Metadata
    public final ArmorType ARMOR_TYPE = ArmorType.Leather;

    // Base stats defensive
    private double baseHealth = CharacterBaseStatsDefensive.ROGUE_BASE_HEALTH;
    private double basePhysReductionPercent = CharacterBaseStatsDefensive.ROGUE_BASE_PHYS_RED; // Armor
    private double baseMagicReductionPercent = CharacterBaseStatsDefensive.ROGUE_BASE_MAGIC_RES; // Magic armor
    // Base stats offensive
    private double baseAttackPower = CharacterBaseStatsOffensive.ROGUE_MELEE_ATTACK_POWER;

    // Active trackers and flags
    private double currentHealth;
    private Boolean isDead = false;
    private Armor armor = null;
    private Weapon weapon = null;
    private double shield = 0;

    // Constructor
    public Rogue() {
        // When the character is created it has maximum health (base health)
        this.currentHealth = baseHealth;
    }

    public Rogue(Armor armor, Weapon weapon) {
        this.currentHealth = baseHealth;
        this.armor = armor;
        this.weapon = weapon;
    }

    // Public getters statuses and stats

    public void setShield(double shieldPoints) {
        shield = shieldPoints;
    }

    public double getCurrentMaxHealth() {
        if (armor == null) {
            return baseHealth;
        } else {
            return baseHealth*armor.getHealthModifier()*armor.getRarityModifier();
        }
    }
    public double getCurrentHealth() {
        return currentHealth;
    }

    public void setCurrentHealth(double healPoints) {
        if (currentHealth + healPoints > this.getCurrentMaxHealth()) {
            currentHealth = this.getCurrentMaxHealth();
        } else {
            currentHealth += healPoints;
        }
    }

    public Boolean getDead() {
        return isDead;
    }

    // Equipment behaviours

    /**
     * Equips armor to the character, modifying stats.
     * @param armor
     */
    public void equipArmor(Armor armor) {
        if (armor instanceof Leather) {
            this.armor = armor;
            this.currentHealth = this.currentHealth*armor.getHealthModifier();
        } else {
            System.out.println("Not eqipped. Rogue only wears leather armor!");
        }
    }

    /**
     * Equips a weapon to the character, modifying stats.
     * @param weapon
     */
    public void equipWeapon(Weapon weapon) {
        if (weapon instanceof Bladed) {
            this.weapon = weapon;
        } else {
            System.out.println("Not equipped. Rogue only uses bladed weapons.");
        }
    }

    // Character behaviours

    /**
     * Damages the enemy
     */
    public double attackWithBladedWeapon() {
        return baseAttackPower*weapon.getModifier()*weapon.getRarity();
    }

    /**
     * Takes damage from an enemy's attack.
     * @param incomingDamage
     * @Param damageType
     */
    public double takeDamage(double incomingDamage, String damageType) {
        double damage = 0;
        if (damageType.equals("physical")) {
            damage = incomingDamage*(1-basePhysReductionPercent*armor.getPhysRedModifier()*armor.getRarityModifier());
        } else if (damageType.equals("magical")) {
            damage = incomingDamage*(1-baseMagicReductionPercent*armor.getMagicRedModifier()*armor.getRarityModifier());
        }
        if (damage < 1 ) {
            damage = 1;
        }
        double curDamage = damage;
        if (shield > 0) {
            if (shield > curDamage) {
                shield -= curDamage;
            } else {
                curDamage -= shield;
                shield = 0;
                currentHealth -= curDamage;
            }
        } else {
            currentHealth -= curDamage;
        }
        if (currentHealth <= 0) {
            isDead = true;
        }
        return damage;
    }
}
