package main.java.characters;
// Imports
import main.java.basestats.CharacterBaseStatsDefensive;
import main.java.basestats.CharacterBaseStatsOffensive;
import main.java.characters.abstractions.RangedChar;
import main.java.items.armor.abstractions.Armor;
import main.java.items.armor.abstractions.ArmorType;
import main.java.items.armor.Mail;
import main.java.items.weapons.abstractions.Weapon;
import main.java.items.weapons.abstractions.Ranged;

/*
 Rangers
 Are masters of ranged combat. They use a wide arsenal of weapons to dispatch enemies.
*/
public class Ranger implements RangedChar {
    // Metadata
    public final ArmorType ARMOR_TYPE = ArmorType.Mail;

    // Base stats defensive
    private double baseHealth = CharacterBaseStatsDefensive.RANGER_BASE_HEALTH;
    private double basePhysReductionPercent = CharacterBaseStatsDefensive.RANGER_BASE_PHYS_RED; // Armor
    private double baseMagicReductionPercent = CharacterBaseStatsDefensive.RANGER_BASE_MAGIC_RES; // Magic armor
    // Base stats offensive
    private double baseAttackPower = CharacterBaseStatsOffensive.RANGER_RANGED_ATTACK_POWER;

    // Active trackers and flags
    private double currentHealth;
    private Boolean isDead = false;
    private Armor armor = null;
    private Weapon weapon = null;
    private double shield = 0;

    // Constructor
    public Ranger() {
        // When the character is created it has maximum health (base health)
        this.currentHealth = baseHealth;
    }

    public Ranger(Armor armor, Weapon weapon) {
        this.currentHealth = baseHealth;
        this.armor = armor;
        this.weapon = weapon;
    }

    // Public getters statuses and stats
    public double getCurrentMaxHealth() {
        if (armor == null) {
            return baseHealth;
        } else {
            return baseHealth*armor.getHealthModifier()*armor.getRarityModifier();
        }
    }

    public void setShield(double shieldPoints) {
        shield = shieldPoints;
    }

    public double getCurrentHealth() {
        return currentHealth;
    }

    public void setCurrentHealth(double healPoints) {
        if (currentHealth + healPoints > this.getCurrentMaxHealth()) {
            currentHealth = this.getCurrentMaxHealth();
        } else {
            currentHealth += healPoints;
        }
    }

    public Boolean getDead() {
        return isDead;
    }

    // Equipment behaviours

    /**
     * Equips armor to the character, modifying stats.
     * @param armor
     */
    public void equipArmor(Armor armor) {
        if (armor instanceof Mail) {
            this.armor = armor;
            this.currentHealth = this.currentHealth*armor.getHealthModifier();
        } else {
            System.out.println("Not eqipped. Ranger only wears Mail armor!");
        }
    }

    /**
     * Equips a weapon to the character, modifying stats.
     * @param weapon
     */
    public void equipWeapon(Weapon weapon) {
        if (weapon instanceof Ranged) {
            this.weapon = weapon;
        } else {
            System.out.println("Not equipped. Ranger only uses ranged weapons.");
        }
    }

    // Character behaviours

    /**
     * Damages the enemy
     */
    public double attackWithRangedWeapon() {
        return baseAttackPower*weapon.getModifier()*weapon.getRarity();
    }

    /**
     * Takes damage from an enemy's attack.
     * @param incomingDamage
     * @Param damageType
     */
    public double takeDamage(double incomingDamage, String damageType) {
        double damage = 0;
        if (damageType.equals("physical")) {
            damage = incomingDamage*(1-basePhysReductionPercent*armor.getPhysRedModifier()*armor.getRarityModifier());
        } else if (damageType.equals("magical")) {
            damage = incomingDamage*(1-baseMagicReductionPercent*armor.getMagicRedModifier()*armor.getRarityModifier());
        }
        if (damage < 1 ) {
            damage = 1;
        }
        double curDamage = damage;
        if (shield > 0) {
            if (shield > curDamage) {
                shield -= curDamage;
            } else {
                curDamage -= shield;
                shield = 0;
                currentHealth -= curDamage;
            }
        } else {
            currentHealth -= curDamage;
        }
        if (currentHealth <= 0) {
            isDead = true;
        }
        return damage;
    }
}
