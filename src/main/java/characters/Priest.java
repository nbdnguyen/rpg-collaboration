package main.java.characters;
// Imports
import main.java.basestats.CharacterBaseStatsDefensive;
import main.java.characters.abstractions.Hero;
import main.java.characters.abstractions.ShieldSupport;
import main.java.items.armor.abstractions.Armor;
import main.java.items.armor.Cloth;
import main.java.items.weapons.abstractions.Weapon;
import main.java.items.weapons.abstractions.Magic;
import main.java.spells.abstractions.ShieldingSpell;

/*
 Priest are the servants of the light and goodness.
 They use holy magic to heal and shield allies.
 As a support class they only have defensive stats.
*/
public class Priest implements ShieldSupport {
    // Metadata
    private ShieldingSpell shieldingSpell;

    // Base stats defensive
    private double baseHealth = CharacterBaseStatsDefensive.PRIEST_BASE_HEALTH;
    private double basePhysReductionPercent = CharacterBaseStatsDefensive.PRIEST_BASE_PHYS_RED; // Armor
    private double baseMagicReductionPercent = CharacterBaseStatsDefensive.PRIEST_BASE_MAGIC_RES; // Magic armor
    // Active trackers and flags
    private double currentHealth;
    private Boolean isDead = false;
    private Armor armor = null;
    private Weapon weapon = null;
    private double shield = 0;

    // Constructor
    public Priest() {
        // When the character is created it has maximum health (base health)
        this.currentHealth = baseHealth;
    }

    public Priest(Armor armor, Weapon weapon, ShieldingSpell shieldingSpell) {
        this.currentHealth = baseHealth;
        this.armor = armor;
        this.weapon = weapon;
        this.shieldingSpell = shieldingSpell;
    }

    // Public getters statuses and stats
    public double getCurrentMaxHealth() {
        if (armor == null) {
            return baseHealth;
        } else {
            return baseHealth*armor.getHealthModifier()*armor.getRarityModifier();
        }
    }

    public double getCurrentHealth() {
        return currentHealth;
    }

    public void setCurrentHealth(double healPoints) {
        if (currentHealth + healPoints > this.getCurrentMaxHealth()) {
            currentHealth = this.getCurrentMaxHealth();
        } else {
            currentHealth += healPoints;
        }
    }

    public void setShield(double shieldPoints) {
        shield = shieldPoints;
    }

    public Boolean getDead() {
        return isDead;
    }

    // Equipment behaviours

    /**
     * Equips armor to the character, modifying stats.
     * @param armor
     */
    public void equipArmor(Armor armor) {
        if (armor instanceof Cloth) {
            this.armor = armor;
            this.currentHealth = this.currentHealth*armor.getHealthModifier();
        } else {
            System.out.println("Not equipped. Priest only wears cloth armor!");
        }
    }

    /**
     * Equips a weapon to the character, modifying stats.
     * @param weapon
     */
    public void equipWeapon(Weapon weapon) {
        if (weapon instanceof Magic) {
            this.weapon = weapon;
        } else {
            System.out.println("Not equipped. Priest only uses magic weapons.");
        }
    }

    // Character behaviours
    /**
     * Shields a party member for a percentage of their maximum health.
     * @param partyMember
     */
    public double shieldPartyMember(Hero partyMember) {
        if (shieldingSpell == null) {
            System.out.println("Priest has no shielding spell!");
            return 0;
        } else {
            double shielding = partyMember.getCurrentMaxHealth()*shieldingSpell.getAbsorbShieldPercentage()
                    + weapon.getModifier()*weapon.getRarity();
            partyMember.setShield(shielding);
            return shielding;
        }
    }

    /**
     * Takes damage from an enemy's attack.
     * @param incomingDamage
     * @Param damageType
     */
    public double takeDamage(double incomingDamage, String damageType) {
        double damage = 0;
        if (damageType.equals("physical")) {
            damage = incomingDamage*(1-basePhysReductionPercent*armor.getPhysRedModifier()*armor.getRarityModifier());
        } else if (damageType.equals("magical")) {
            damage = incomingDamage*(1-baseMagicReductionPercent*armor.getMagicRedModifier()*armor.getRarityModifier());
        }
        if (damage < 1 ) {
            damage = 1;
        }
        double curDamage = damage;
        if (shield > 0) {
            if (shield > curDamage) {
                shield -= curDamage;
            } else {
                curDamage -= shield;
                shield = 0;
                currentHealth -= curDamage;
            }
        } else {
            currentHealth -= curDamage;
        }
        if (currentHealth <= 0) {
            isDead = true;
        }
        return damage;
    }
}
