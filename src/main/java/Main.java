package main.java;

import main.java.basestats.ItemRarityModifiers;
import main.java.characters.*;
import main.java.characters.abstractions.CharacterType;
import main.java.characters.abstractions.Hero;
import main.java.factories.ArmorFactory;
import main.java.factories.CharacterFactory;
import main.java.factories.WeaponFactory;
import main.java.items.armor.abstractions.Armor;
import main.java.items.armor.abstractions.ArmorType;
import main.java.items.weapons.abstractions.Weapon;
import main.java.items.weapons.abstractions.WeaponType;
import main.java.items.weapons.magic.Staff;

import java.util.ArrayList;
import java.util.Arrays;

import static main.java.basestats.ItemRarityModifiers.EPIC_RARITY_MODIFIER;
import static main.java.basestats.ItemRarityModifiers.RARE_RARITY_MODIFIER;

public class Main {
    public static void main(String[] args) {
        CharacterFactory charFac = new CharacterFactory();
        Druid druid = (Druid) charFac.getCharacter(CharacterType.Druid);
        Mage mage = (Mage) charFac.getCharacter(CharacterType.Mage);
        Paladin paladin = (Paladin) charFac.getCharacter(CharacterType.Paladin);
        Priest priest = (Priest) charFac.getCharacter(CharacterType.Priest);
        Ranger ranger = (Ranger) charFac.getCharacter(CharacterType.Ranger);
        Rogue rogue = (Rogue) charFac.getCharacter(CharacterType.Rogue);
        Warlock warlock = (Warlock) charFac.getCharacter(CharacterType.Warlock);
        Warrior warrior = (Warrior) charFac.getCharacter(CharacterType.Warrior);

        WeaponFactory wepFac = new WeaponFactory();
        Weapon legenCross = wepFac.getWeapon(WeaponType.Crossbow, ItemRarityModifiers.LEGENDARY_RARITY_MODIFIER);

        ArmorFactory armFac = new ArmorFactory();
        Armor epicCloth = armFac.getArmor(ArmorType.Cloth, EPIC_RARITY_MODIFIER);

        System.out.println("--- All characters attacks the air!! ---");
        System.out.println("Druid heals the air with " + druid.healPartyMember(druid) + "hp.");
        System.out.println("Mage attacks the air with " + mage.castDamagingSpell() + "hp.");
        System.out.println("Paladin attacks the air with " + paladin.attackWithBluntWeapon() + "hp.");
        System.out.println("Priest shields the air with " + priest.shieldPartyMember(ranger) + "hp.");
        System.out.println("Ranger attacks the air with " + ranger.attackWithRangedWeapon() + "hp.");
        System.out.println("Rogue attacks the air with " + rogue.attackWithBladedWeapon() + "hp.");
        System.out.println("Warlock attacks the air with " + warlock.castDamagingSpell() + "hp.");
        System.out.println("Warrior attacks the air with " + warrior.attackWithBladedWeapon() + "hp.");
        System.out.println("All was a critical hit, air is now dead.");

        System.out.println();
        System.out.println("--- A character receives 100 damage in physical and magical. ---");
        System.out.println("Physical damage taken by warrior: "
                + warrior.takeDamage(100, "physical"));
        System.out.println("Magical damage taken by warrior: "
                + warrior.takeDamage(100,"magical"));

        System.out.println();
        double attack = ranger.attackWithRangedWeapon();
        System.out.println("--- Attacks with different armor and weapons. ---");
        System.out.println("--- Basic attack. ---");
        System.out.println("Ranger attacks the priest with " + attack + "p of damage.");
        System.out.println("Priest takes " + priest.takeDamage(attack, "physical") + "p of damage.");
        System.out.println("Priest now has " + priest.getCurrentHealth() + "hp.");
        System.out.println();

        double shield = priest.shieldPartyMember(priest);
        priest.equipArmor(epicCloth);
        System.out.println("--- Taking damage with better armor and showing shield effect. ---");
        System.out.println("Priest shields themselves with a " + shield + "p shield and equipped epic cloth.");
        attack = ranger.attackWithRangedWeapon();
        System.out.println("Ranger attacks the priest again with " + attack + "p of damage.");
        System.out.println("Priest takes " + priest.takeDamage(attack, "physical") + "p of damage.");
        System.out.println("Priest now has " + priest.getCurrentHealth() + "hp.");

        System.out.println();
        System.out.println("--- Attacking with better weapon and showing heal effect. ---");
        System.out.println("Druid heals priest with " + druid.healPartyMember(priest) + "hp.");
        ranger.equipWeapon(legenCross);
        attack = ranger.attackWithRangedWeapon();
        System.out.println("Ranger equips a legendary crossbow and attacks the priest again with "
                + attack + "p of damage.");
        System.out.println("The priest has " + priest.getCurrentHealth() + "hp and takes "
                + priest.takeDamage(attack, "physical") + "p of damage and now has "
                + priest.getCurrentHealth() + "hp left.");
        if (priest.getDead()) {
            System.out.println("The priest is now dead.");
        }

        System.out.println();
        System.out.println("--- Equipping wrong armor and weapon. ---");
        druid.equipWeapon(legenCross);
        paladin.equipArmor(epicCloth);
    }
}
