package main.java.items.weapons.magic;

import main.java.basestats.WeaponStatsModifiers;
import main.java.items.weapons.abstractions.Magic;

import static main.java.basestats.ItemRarityModifiers.COMMON_RARITY_MODIFIER;

public class Wand implements Magic {
    // Stat modifiers
    private double magicPowerModifier = WeaponStatsModifiers.WAND_MAGIC_MOD;
    // Rarity
    private double rarityModifier;

    // Public properties
    public double getRarity() {
        return rarityModifier;
    }

    public double getModifier() { return magicPowerModifier; }

    // Constructors
    public Wand() {
        this.rarityModifier = COMMON_RARITY_MODIFIER;
    }

    public Wand(double rarity) {
        this.rarityModifier = rarity;
    }
}
