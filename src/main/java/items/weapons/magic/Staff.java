package main.java.items.weapons.magic;

import main.java.basestats.WeaponStatsModifiers;
import main.java.items.weapons.abstractions.Magic;

import static main.java.basestats.ItemRarityModifiers.COMMON_RARITY_MODIFIER;

public class Staff implements Magic {
    // Stat modifiers
    private double magicPowerModifier = WeaponStatsModifiers.STAFF_MAGIC_MOD;
    // Rarity
    private double rarityModifier;

    // Public properties
    public double getRarity() {
        return rarityModifier;
    }

    public double getModifier() { return magicPowerModifier; }

    // Constructors
    public Staff() {
        this.rarityModifier = COMMON_RARITY_MODIFIER;
    }

    public Staff(double rarity) {
        this.rarityModifier = rarity;
    }
}
