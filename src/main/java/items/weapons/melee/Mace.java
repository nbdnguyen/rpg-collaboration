package main.java.items.weapons.melee;

import main.java.basestats.WeaponStatsModifiers;
import main.java.items.weapons.abstractions.Blunt;

import static main.java.basestats.ItemRarityModifiers.COMMON_RARITY_MODIFIER;

public class Mace implements Blunt {
    // Stat modifiers
    private double attackPowerModifier = WeaponStatsModifiers.MACE_ATTACK_MOD;
    // Rarity
    private double rarityModifier;

    // Public properties
    public double getRarity() {
        return rarityModifier;
    }

    public double getModifier() { return attackPowerModifier; }

    // Constructors
    public Mace() {
        this.rarityModifier = COMMON_RARITY_MODIFIER;
    }

    public Mace(double rarity) {
        this.rarityModifier = rarity;
    }
}
