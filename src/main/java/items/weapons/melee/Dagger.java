package main.java.items.weapons.melee;

import main.java.basestats.WeaponStatsModifiers;
import main.java.items.weapons.abstractions.Bladed;

import static main.java.basestats.ItemRarityModifiers.COMMON_RARITY_MODIFIER;

public class Dagger implements Bladed {
    // Stat modifiers
    private double attackPowerModifier = WeaponStatsModifiers.DAGGER_ATTACK_MOD;
    // Rarity
    private double rarityModifier;

    // Public properties
    public double getRarity() {
        return rarityModifier;
    }

    public double getModifier() { return attackPowerModifier; }

    // Constructors
    public Dagger() {
        this.rarityModifier = COMMON_RARITY_MODIFIER;
    }

    public Dagger(double rarity) {
        this.rarityModifier = rarity;
    }
}
