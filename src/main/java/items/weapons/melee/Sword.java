package main.java.items.weapons.melee;

import main.java.basestats.WeaponStatsModifiers;
import main.java.items.weapons.abstractions.Bladed;

import static main.java.basestats.ItemRarityModifiers.COMMON_RARITY_MODIFIER;

public class Sword implements Bladed {
    // Stat modifiers
    private double attackPowerModifier = WeaponStatsModifiers.SWORD_ATTACK_MOD;
    // Rarity
    private double rarityModifier;

    // Public properties
    public double getRarity() {
        return rarityModifier;
    }

    public double getModifier() { return attackPowerModifier; }

    // Constructors
    public Sword() {
        this.rarityModifier = COMMON_RARITY_MODIFIER;
    }

    public Sword(double rarity) {
        this.rarityModifier = rarity;
    }
}
