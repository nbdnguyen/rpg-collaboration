package main.java.items.weapons.melee;

import main.java.basestats.WeaponStatsModifiers;
import main.java.items.weapons.abstractions.Bladed;

import static main.java.basestats.ItemRarityModifiers.COMMON_RARITY_MODIFIER;

public class Axe implements Bladed {
    // Stat modifiers
    private double attackPowerModifier = WeaponStatsModifiers.AXE_ATTACK_MOD;
    // Rarity
    private double rarityModifier;

    // Public properties
    public double getRarity() {
        return rarityModifier;
    }

    public double getModifier() { return attackPowerModifier; }

    // Constructors
    public Axe() {
        this.rarityModifier = COMMON_RARITY_MODIFIER;
    }

    public Axe(double rarity) {
        this.rarityModifier = rarity;
    }
}
