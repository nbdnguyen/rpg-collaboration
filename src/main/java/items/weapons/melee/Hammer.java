package main.java.items.weapons.melee;

import main.java.basestats.WeaponStatsModifiers;
import main.java.items.weapons.abstractions.Blunt;

import static main.java.basestats.ItemRarityModifiers.COMMON_RARITY_MODIFIER;

public class Hammer implements Blunt {
    // Stat modifiers
    private double attackPowerModifier = WeaponStatsModifiers.HAMMER_ATTACK_MOD;
    // Rarity
    private double rarityModifier;

    // Public properties
    public double getRarity() {
        return rarityModifier;
    }

    public double getModifier() { return attackPowerModifier; }

    // Constructors
    public Hammer() {
        this.rarityModifier = COMMON_RARITY_MODIFIER;
    }

    public Hammer(double rarity) {
        this.rarityModifier = rarity;
    }
}
