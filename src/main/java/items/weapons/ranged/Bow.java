package main.java.items.weapons.ranged;

import main.java.basestats.WeaponStatsModifiers;
import main.java.items.weapons.abstractions.Ranged;

import static main.java.basestats.ItemRarityModifiers.COMMON_RARITY_MODIFIER;

public class Bow implements Ranged {

    // Stat modifiers
    private double attackPowerModifier = WeaponStatsModifiers.BOW_ATTACK_MOD;
    //Rarity
    private final double rarityModifier;

    public Bow() { this.rarityModifier = COMMON_RARITY_MODIFIER; }
    public Bow(double rarity) {
        this.rarityModifier = rarity;
    }

    // Public properties
    public double getRarity() {
        return rarityModifier;
    }

    public double getModifier() { return attackPowerModifier; }
}
