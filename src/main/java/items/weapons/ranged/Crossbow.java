package main.java.items.weapons.ranged;

import main.java.basestats.WeaponStatsModifiers;
import main.java.items.weapons.abstractions.Ranged;

import static main.java.basestats.ItemRarityModifiers.COMMON_RARITY_MODIFIER;

public class Crossbow implements Ranged {
    // Stat modifiers
    private double attackPowerModifier = WeaponStatsModifiers.CROSSBOW_ATTACK_MOD;
    // Rarity
    private double rarityModifier;

    // Public properties
    public double getRarity() {
        return rarityModifier;
    }

    public double getModifier() { return attackPowerModifier; }

    // Constructors
    public Crossbow() {
        this.rarityModifier = COMMON_RARITY_MODIFIER;
    }

    public Crossbow(double rarity) {
        this.rarityModifier = rarity;
    }
}
