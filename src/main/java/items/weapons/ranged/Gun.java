package main.java.items.weapons.ranged;

import main.java.basestats.WeaponStatsModifiers;
import main.java.items.weapons.abstractions.Ranged;

import static main.java.basestats.ItemRarityModifiers.COMMON_RARITY_MODIFIER;

public class Gun implements Ranged {
    // Stat modifiers
    private double attackPowerModifier = WeaponStatsModifiers.GUN_ATTACK_MOD;
    // Rarity
    private double rarityModifier;

    // Public properties
    public double getRarity() {
        return rarityModifier;
    }

    public double getModifier() { return attackPowerModifier; }

    // Constructors
    public Gun() {
        this.rarityModifier = COMMON_RARITY_MODIFIER;
    }

    public Gun(double rarity) {
        this.rarityModifier = rarity;
    }
}
