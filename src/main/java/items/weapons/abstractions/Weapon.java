package main.java.items.weapons.abstractions;

public interface Weapon {

    // Public properties
    double getRarity();

    double getModifier();

}