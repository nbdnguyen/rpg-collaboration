package main.java.items.armor.abstractions;

import main.java.basestats.ArmorStatsModifiers;

public interface Armor {

    double getHealthModifier();

    double getPhysRedModifier();

    double getMagicRedModifier();

    double getRarityModifier();
}
