package main.java.items.armor;

import main.java.basestats.ArmorStatsModifiers;
import main.java.items.armor.abstractions.Armor;

import static main.java.basestats.ItemRarityModifiers.COMMON_RARITY_MODIFIER;

public class Leather implements Armor {
    // Stat modifiers
    private double healthModifier = ArmorStatsModifiers.LEATHER_HEALTH_MODIFIER;
    private double physRedModifier = ArmorStatsModifiers.LEATHER_PHYS_RED_MODIFIER;
    private double magicRedModifier = ArmorStatsModifiers.LEATHER_MAGIC_RED_MODIFIER;
    // Rarity
    private final double rarityModifier;

    // Public properties
    public double getHealthModifier() {
        return healthModifier;
    }

    public double getPhysRedModifier() {
        return physRedModifier;
    }

    public double getMagicRedModifier() {
        return magicRedModifier;
    }

    public double getRarityModifier() {
        return rarityModifier;
    }

    // Constructors
    public Leather() { this.rarityModifier = COMMON_RARITY_MODIFIER; }
    public Leather(double itemRarity) {
        this.rarityModifier = itemRarity;
    }
}
