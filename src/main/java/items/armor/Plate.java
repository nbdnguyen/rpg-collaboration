package main.java.items.armor;

import main.java.basestats.ArmorStatsModifiers;
import main.java.items.armor.abstractions.Armor;

import static main.java.basestats.ItemRarityModifiers.COMMON_RARITY_MODIFIER;

public class Plate implements Armor {
    // Stat modifiers
    private double healthModifier = ArmorStatsModifiers.PLATE_HEALTH_MODIFIER;
    private double physRedModifier = ArmorStatsModifiers.PLATE_PHYS_RED_MODIFIER;
    private double magicRedModifier = ArmorStatsModifiers.PLATE_MAGIC_RED_MODIFIER;
    // Rarity
    private final double rarityModifier;

    // Public properties
    public double getHealthModifier() {
        return healthModifier;
    }

    public double getPhysRedModifier() {
        return physRedModifier;
    }

    public double getMagicRedModifier() {
        return magicRedModifier;
    }

    public double getRarityModifier() {
        return rarityModifier;
    }

    // Constructors
    public Plate() {this.rarityModifier = COMMON_RARITY_MODIFIER; }
    public Plate(double itemRarity) {
        this.rarityModifier = itemRarity;
    }
}
