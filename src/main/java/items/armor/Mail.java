package main.java.items.armor;

import main.java.basestats.ArmorStatsModifiers;
import main.java.items.armor.abstractions.Armor;

import static main.java.basestats.ItemRarityModifiers.COMMON_RARITY_MODIFIER;

public class Mail implements Armor {
    // Stat modifiers
    private double healthModifier = ArmorStatsModifiers.MAIL_HEALTH_MODIFIER;
    private double physRedModifier = ArmorStatsModifiers.MAIL_PHYS_RED_MODIFIER;
    private double magicRedModifier = ArmorStatsModifiers.MAIL_MAGIC_RED_MODIFIER;
    // Rarity
    private final double rarityModifier;

    // Public properties
    public double getHealthModifier() {
        return healthModifier;
    }

    public double getPhysRedModifier() {
        return physRedModifier;
    }

    public double getMagicRedModifier() {
        return magicRedModifier;
    }

    public double getRarityModifier() {
        return rarityModifier;
    }

    // Constructors
    public Mail() { this.rarityModifier = COMMON_RARITY_MODIFIER; }
    public Mail(double itemRarity) {
        this.rarityModifier = itemRarity;
    }
}
