package main.java.factories;
// Imports
import main.java.characters.abstractions.CharacterType;
import main.java.characters.abstractions.Hero;
import main.java.characters.Druid;
import main.java.characters.Mage;
import main.java.characters.Paladin;
import main.java.characters.Priest;
import main.java.characters.Ranger;
import main.java.characters.Rogue;
import main.java.characters.Warlock;
import main.java.characters.Warrior;
import main.java.items.armor.Cloth;
import main.java.items.armor.Leather;
import main.java.items.armor.Mail;
import main.java.items.armor.Plate;
import main.java.items.weapons.abstractions.Magic;
import main.java.items.weapons.magic.Staff;
import main.java.items.weapons.melee.Axe;
import main.java.items.weapons.melee.Hammer;
import main.java.items.weapons.ranged.Bow;
import main.java.spells.abstractions.DamagingSpell;
import main.java.spells.abstractions.HealingSpell;
import main.java.spells.abstractions.ShieldingSpell;
import main.java.spells.damaging.ArcaneMissile;
import main.java.spells.damaging.ChaosBolt;
import main.java.spells.healing.Regrowth;
import main.java.spells.shielding.Barrier;

/*
 This factory exists to be responsible for creating new heroes.
*/
public class CharacterFactory {
    Leather leather = new Leather();
    Mail mail = new Mail();
    Cloth cloth = new Cloth();
    Plate plate = new Plate();

    Staff staff = new Staff();
    Hammer hammer = new Hammer();
    Bow bow = new Bow();
    Axe axe = new Axe();

    public Hero getCharacter(CharacterType characterType) {
        //Characters created with common items.
        switch(characterType) {
            case Druid:
                HealingSpell regrowth = new Regrowth();
                return new Druid(leather, staff, regrowth);
            case Mage:
                DamagingSpell arcane = new ArcaneMissile();
                return new Mage(cloth, staff, arcane);
            case Paladin:
                return new Paladin(plate, hammer);
            case Priest:
                ShieldingSpell barrier = new Barrier();
                return new Priest(cloth, staff, barrier);
            case Ranger:
                return new Ranger(mail, bow);
            case Rogue:
                return new Rogue(leather, axe);
            case Warlock:
                DamagingSpell chaos = new ChaosBolt();
                return new Warlock(cloth, staff, chaos);
            case Warrior:
                return new Warrior(plate, axe);
            default:
                return null;
        }
    }
}
