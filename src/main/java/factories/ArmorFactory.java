package main.java.factories;
// Imports
import main.java.items.armor.*;
import main.java.items.armor.abstractions.Armor;
import main.java.items.armor.abstractions.ArmorType;

/*
 This factory exists to be responsible for creating new Armor.
 Object is replaced with some Item abstraction.
*/
public class ArmorFactory {

    public Armor getArmor(ArmorType armorType) {
        //If no rarity is given, the item will be common rarity, set in the armor constructor.
        switch(armorType) {
            case Cloth:
                return new Cloth();
            case Leather:
                return new Leather();
            case Mail:
                return new Mail();
            case Plate:
                return new Plate();
            default:
                return null;
        }
    }
    public Armor getArmor(ArmorType armorType, double itemRarityModifier){
        switch(armorType) {
            case Cloth:
                return new Cloth(itemRarityModifier);
            case Leather:
                return new Leather(itemRarityModifier);
            case Mail:
                return new Mail(itemRarityModifier);
            case Plate:
                return new Plate(itemRarityModifier);
            default:
                return null;
        }
    }

}
