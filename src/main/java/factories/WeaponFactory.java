package main.java.factories;
// Imports
import main.java.items.weapons.abstractions.Weapon;
import main.java.items.weapons.abstractions.WeaponType;
import main.java.items.weapons.magic.Staff;
import main.java.items.weapons.magic.Wand;
import main.java.items.weapons.melee.*;
import main.java.items.weapons.ranged.Bow;
import main.java.items.weapons.ranged.Crossbow;
import main.java.items.weapons.ranged.Gun;
/*
 This factory exists to be responsible for creating new enemies.
 Object is replaced with Weapon as a return type when refactored to be good OO design.
*/
public class WeaponFactory {
    public Weapon getWeapon(WeaponType weaponType) {
        switch(weaponType) {
            case Axe:
                return new Axe();
            case Bow:
                return new Bow();
            case Crossbow:
                return new Crossbow();
            case Dagger:
                return new Dagger();
            case Gun:
                return new Gun();
            case Hammer:
                return new Hammer();
            case Mace:
                return new Mace();
            case Staff:
                return new Staff();
            case Sword:
                return new Sword();
            case Wand:
                return new Wand();
            default:
                return null;
        }
    }
    public Weapon getWeapon(WeaponType weaponType, double itemRarityModifier) {
        switch(weaponType) {
            case Axe:
                return new Axe(itemRarityModifier);
            case Bow:
                return new Bow(itemRarityModifier);
            case Crossbow:
                return new Crossbow(itemRarityModifier);
            case Dagger:
                return new Dagger(itemRarityModifier);
            case Gun:
                return new Gun(itemRarityModifier);
            case Hammer:
                return new Hammer(itemRarityModifier);
            case Mace:
                return new Mace(itemRarityModifier);
            case Staff:
                return new Staff(itemRarityModifier);
            case Sword:
                return new Sword(itemRarityModifier);
            case Wand:
                return new Wand(itemRarityModifier);
            default:
                return null;
        }
    }
}
