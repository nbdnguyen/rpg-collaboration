package main.test;

import main.java.characters.Druid;
import main.java.characters.abstractions.CharacterType;
import main.java.characters.abstractions.Hero;
import main.java.factories.CharacterFactory;
import org.junit.jupiter.api.Test;

import static main.java.basestats.ArmorStatsModifiers.MAIL_HEALTH_MODIFIER;
import static main.java.basestats.CharacterBaseStatsDefensive.RANGER_BASE_HEALTH;
import static org.junit.jupiter.api.Assertions.*;

class HealTest {
    CharacterFactory charFac = new CharacterFactory();
    Druid druid = (Druid) charFac.getCharacter(CharacterType.Druid);
    Hero randomChar = charFac.getCharacter(CharacterType.Ranger);

    @Test
    void healTest() {
        randomChar.takeDamage(50,"physical");
        druid.healPartyMember(randomChar);
        assertEquals(RANGER_BASE_HEALTH*MAIL_HEALTH_MODIFIER, randomChar.getCurrentHealth());
    }
}