package main.test;

import main.java.characters.Paladin;
import main.java.characters.Ranger;
import main.java.characters.Rogue;
import main.java.characters.Warrior;
import main.java.characters.abstractions.CharacterType;
import main.java.factories.CharacterFactory;
import org.junit.jupiter.api.Test;

import static main.java.basestats.CharacterBaseStatsDefensive.*;
import static org.junit.jupiter.api.Assertions.*;

public class DamageTest {
    CharacterFactory charFac = new CharacterFactory();
    Ranger ranger = (Ranger) charFac.getCharacter(CharacterType.Ranger);
    Paladin paladin = (Paladin) charFac.getCharacter(CharacterType.Paladin);
    Rogue rogue = (Rogue) charFac.getCharacter(CharacterType.Rogue);
    Warrior warrior = (Warrior) charFac.getCharacter(CharacterType.Warrior);

    @Test
    void damageTest() {
        ranger.takeDamage(100, "physical");
        paladin.takeDamage(100, "physical");
        rogue.takeDamage(100, "physical");
        warrior.takeDamage(100, "physical");
        assertNotEquals(RANGER_BASE_HEALTH, ranger.getCurrentHealth());
        assertNotEquals(PALADIN_BASE_HEALTH, ranger.getCurrentHealth());
        assertNotEquals(ROGUE_BASE_HEALTH, ranger.getCurrentHealth());
        assertNotEquals(WARRIOR_BASE_HEALTH, ranger.getCurrentHealth());
    }
    @Test
    void attackTest() {
        paladin.takeDamage(ranger.attackWithRangedWeapon(), "physical");
        rogue.takeDamage(paladin.attackWithBluntWeapon(), "physical");
        warrior.takeDamage(rogue.attackWithBladedWeapon(), "physical");
        ranger.takeDamage(warrior.attackWithBladedWeapon(), "physical");
        assertNotEquals(RANGER_BASE_HEALTH, ranger.getCurrentHealth());
        assertNotEquals(PALADIN_BASE_HEALTH, ranger.getCurrentHealth());
        assertNotEquals(ROGUE_BASE_HEALTH, ranger.getCurrentHealth());
        assertNotEquals(WARRIOR_BASE_HEALTH, ranger.getCurrentHealth());
    }
}
