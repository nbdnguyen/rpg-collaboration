package main.test;
import main.java.characters.Mage;
import main.java.characters.Warlock;
import main.java.characters.abstractions.CharacterType;
import main.java.factories.CharacterFactory;
import org.junit.jupiter.api.Test;

import static main.java.basestats.CharacterBaseStatsDefensive.MAGE_BASE_HEALTH;
import static main.java.basestats.CharacterBaseStatsDefensive.WARLOCK_BASE_HEALTH;
import static org.junit.jupiter.api.Assertions.*;

public class CasterTest {
    CharacterFactory charFac = new CharacterFactory();
    Mage mage = (Mage) charFac.getCharacter(CharacterType.Mage);
    Warlock warlock = (Warlock) charFac.getCharacter(CharacterType.Warlock);

    @Test
    void casterTest() {
        warlock.takeDamage(mage.castDamagingSpell(), "magical");
        assertNotEquals(WARLOCK_BASE_HEALTH, warlock.getCurrentHealth());
        mage.takeDamage(warlock.castDamagingSpell(), "magical");
        assertNotEquals(MAGE_BASE_HEALTH, "magical");
    }
}
