package main.test;

import main.java.characters.Priest;
import main.java.characters.abstractions.CharacterType;
import main.java.characters.abstractions.Hero;
import main.java.factories.CharacterFactory;
import org.junit.jupiter.api.Test;

import static main.java.basestats.ArmorStatsModifiers.MAIL_HEALTH_MODIFIER;
import static main.java.basestats.CharacterBaseStatsDefensive.RANGER_BASE_HEALTH;
import static org.junit.jupiter.api.Assertions.*;

class ShieldTest {
    CharacterFactory charFac = new CharacterFactory();
    Priest priest = (Priest) charFac.getCharacter(CharacterType.Priest);
    Hero randomChar = charFac.getCharacter(CharacterType.Ranger);

    @Test
    void shieldTest() {
        priest.shieldPartyMember(randomChar);
        randomChar.takeDamage(100,"physical");
        assertEquals(RANGER_BASE_HEALTH, randomChar.getCurrentHealth());
        randomChar.takeDamage(1,"physical");
        assertEquals(RANGER_BASE_HEALTH, randomChar.getCurrentHealth());
    }
}