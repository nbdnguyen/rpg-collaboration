package main.test;

import main.java.characters.abstractions.CharacterType;
import main.java.factories.ArmorFactory;
import main.java.factories.WeaponFactory;
import main.java.items.armor.abstractions.Armor;
import main.java.items.armor.abstractions.ArmorType;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

import main.java.characters.abstractions.Hero;
import main.java.factories.CharacterFactory;

import static main.java.basestats.ArmorStatsModifiers.*;
import static main.java.basestats.CharacterBaseStatsDefensive.*;

public class CharacterTest {

    private CharacterFactory charFac = new CharacterFactory();

    @Test
    public void testDruid() {
        Hero hero = charFac.getCharacter(CharacterType.Druid);
        assertEquals(DRUID_BASE_HEALTH, hero.getCurrentHealth());
        assertEquals(DRUID_BASE_HEALTH*LEATHER_HEALTH_MODIFIER, hero.getCurrentMaxHealth());
        assertFalse(hero.getDead());
    }
    @Test
    public void testMage() {
        Hero hero = charFac.getCharacter(CharacterType.Mage);
        assertEquals(MAGE_BASE_HEALTH, hero.getCurrentHealth());
        assertEquals(MAGE_BASE_HEALTH*CLOTH_HEALTH_MODIFIER, hero.getCurrentMaxHealth());
        assertFalse(hero.getDead());
    }
    @Test
    public void testPaladin() {
        Hero hero = charFac.getCharacter(CharacterType.Paladin);
        assertEquals(PALADIN_BASE_HEALTH, hero.getCurrentHealth());
        assertEquals(PALADIN_BASE_HEALTH*PLATE_HEALTH_MODIFIER, hero.getCurrentMaxHealth());
        assertFalse(hero.getDead());
    }
    @Test
    public void testPriest() {
        Hero hero = charFac.getCharacter(CharacterType.Priest);
        assertEquals(PRIEST_BASE_HEALTH, hero.getCurrentHealth());
        assertEquals(PRIEST_BASE_HEALTH*CLOTH_HEALTH_MODIFIER, hero.getCurrentMaxHealth());
        assertFalse(hero.getDead());
    }
    @Test
    public void testRanger() {
        Hero hero = charFac.getCharacter(CharacterType.Ranger);
        assertEquals(RANGER_BASE_HEALTH, hero.getCurrentHealth());
        assertEquals(RANGER_BASE_HEALTH*MAIL_HEALTH_MODIFIER, hero.getCurrentMaxHealth());
        assertFalse(hero.getDead());
    }
    @Test
    public void testRogue() {
        Hero hero = charFac.getCharacter(CharacterType.Rogue);
        assertEquals(ROGUE_BASE_HEALTH, hero.getCurrentHealth());
        assertEquals(ROGUE_BASE_HEALTH*LEATHER_HEALTH_MODIFIER, hero.getCurrentMaxHealth());
        assertFalse(hero.getDead());
    }
    @Test
    public void testWarlock() {
        Hero hero = charFac.getCharacter(CharacterType.Warlock);
        assertEquals(WARLOCK_BASE_HEALTH, hero.getCurrentHealth());
        assertEquals(WARLOCK_BASE_HEALTH*CLOTH_HEALTH_MODIFIER, hero.getCurrentMaxHealth());
        assertFalse(hero.getDead());
    }
    @Test
    public void testWarrior() {
        Hero hero = charFac.getCharacter(CharacterType.Warrior);
        assertEquals(WARRIOR_BASE_HEALTH, hero.getCurrentHealth());
        assertEquals(WARRIOR_BASE_HEALTH*PLATE_HEALTH_MODIFIER, hero.getCurrentMaxHealth());
        assertFalse(hero.getDead());
    }
}